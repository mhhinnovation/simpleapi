# SIMPLE API #

This is a simple API to manage customers.
It is also support Swagger UI and covered with basic unit tests.

### Functions

A simple API that allows:

-	Adding customers
	
	-		First name
	-		last name
	- 		date of birth fields.

-	Editing customers

-	Deleting customers

-	Searching for a customer by partial name match (first or last name).

### Tech

-	ASP.NET Core 2.2 API
-	In memory entity framework store
-	Dependency injection
-	Basic XUnit tests
-	Swagger / OpenAPI support


### How do I get set up? ###

- Make sure your machine have ASP.NET Core 2.2 Installed
- Clone the project or download to local machine and load with Visual Studio (recommend VS2019)
- Rebuild the project and make sure all required packages are installed
- Set SimpleAPI project as startup project.
- Run or Debug the application.
- A Swagger web page will be opened with default browser (recommend Chrome)
	-	 	The url will be automatically set to http://localhost:56717/index.html
	-		If the port are used on your machine, you may need to change the launch config to different port.


### Try it out with swagger

- The application required no authentication or authorization.

- To try it out, click on the endpoint , then the button "Try It Out"

 The database will be empty. Order of the endpoints I would recommend to try are :

1. Create Endpoints

2. Any Get Endpoints

3. Update Endpoints

4. Delete Endpoints 


** Id type are GUID.

** Assume: Last name and first name are required while Date of birth are optional, Names are not unique.

### Run Automation Tests

- Right click on the SimpleAPI.Tests project
- Select Run or Debug Unit Tests
- These tests are categorised with Traits Attribute, to view, please group by "Traits" on the Unit test Explorer.

** As this project are to demo purpose and time constraints, only basic test are covered.

** As the scope of the tests are not mention so I write the unit tests for both service and controller.

