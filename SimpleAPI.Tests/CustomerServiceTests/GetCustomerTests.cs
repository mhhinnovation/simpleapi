using SimpleAPI.Exceptions;
using SimpleAPI.Interfaces;
using SimpleAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using SimpleAPI.Constants;
using SimpleAPI.Services;
using Xunit;
namespace SimpleAPI.Tests.CustomerServiceTests
{
    public class GetCustomerTests: CustomerServiceTestBase
    {
        private  Guid _mockExistingCustomerId;

        private void MockCustomerList(ICustomerService customerService)
        {
            _mockExistingCustomerId = Guid.NewGuid();

            customerService.CreateAsync(new Customer { FirstName = "Alice", LastName = "Smith"});

            customerService.CreateAsync(new Customer { FirstName = "Alex", LastName = "Wilson"});

            customerService.CreateAsync(new Customer { Id = _mockExistingCustomerId, FirstName = "Miranda", LastName = "Huynh"});
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.GetAllAsync))]
        public async Task GetAllAsync_ReturnsListOfAllCustomer()
        {
            //Arrange
            var customerService = MockNewCustomerService();
            MockCustomerList(customerService);
            
            // Act
            var result = await customerService.GetAllAsync();
         
            // Assert
            Assert.Equal(3, result.Count());
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.GetByIdAsync))]
        public async Task GetByIdAsync_GivenExistingId_ReturnsOneCustomer()
        {
            //Arrange
            var customerService = MockNewCustomerService();
            MockCustomerList(customerService);

            // Act
            var result = await customerService.GetByIdAsync(_mockExistingCustomerId);

            // Assert
            Assert.NotNull( result);
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.GetByIdAsync))]
        public async Task GetByIdAsync_GivenNotExistingId_ReturnsNoCustomer()
        {
            //Arrange
            var customerService = MockNewCustomerService();
            MockCustomerList(customerService);

            // Act
            var result = await customerService.GetByIdAsync(new Guid());

            // Assert
            Assert.Null(result);
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.GetByIdAsync))]
        public async Task GetByIdAsync_GivenEmptyId_ReturnsNoCustomer()
        {
            //Arrange
            var customerService = MockNewCustomerService();
            MockCustomerList(customerService);

            // Act
            var result = await customerService.GetByIdAsync(Guid.Empty);

            // Assert
            Assert.Null(result);
        }

        [Theory]
        [Trait(nameof(CustomerService), nameof(CustomerService.GetByNameAsync))]
        [InlineData("Alice", 1)]
        [InlineData("Al", 2)]
        [InlineData("mi", 2)]
        [InlineData("AliCE Smith", 1)]
        public async Task GetByNameAsync_GivenValidKeyword_ReturnsListOfCustomer(string name, int expectCount)
        {
            //Arrange
            var customerService = MockNewCustomerService();
            MockCustomerList(customerService);

            // Act
            var result = await customerService.GetByNameAsync(name);

            // Assert
            var customerList = result.ToList();

            Assert.True(customerList.Any());
            Assert.Equal(expectCount, customerList.Count());
        }
        
        [Theory]
        [Trait(nameof(CustomerService), nameof(CustomerService.GetByNameAsync))]
        [InlineData("Abc")]
        [InlineData("123")]
        public async Task GetByNameAsync_GivenInvalidKeyword_ReturnsEmptyList(string name)
        {
            //Arrange
            var customerService = MockNewCustomerService();
            MockCustomerList(customerService);

            // Act
            var result = await customerService.GetByNameAsync(name);

            // Assert
            var customerList = result.ToList();

            Assert.Empty(customerList);
        }

        [Theory]
        [Trait(nameof(CustomerService), nameof(CustomerService.GetByNameAsync))]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetByNameAsync_GivenEmptyKeyword_ThrowsException(string name)
        {
            //Arrange
            var customerService = MockNewCustomerService();
            MockCustomerList(customerService);

            // Act && assert
            var exception =
                await Assert.ThrowsAsync<ServiceException>(() => customerService.GetByNameAsync(name));

            Assert.Contains(Errors.NameForSearchingAreRequired, exception.Message);
        }
    }
}
