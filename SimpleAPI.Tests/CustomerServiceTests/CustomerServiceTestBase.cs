﻿using SimpleAPI.Interfaces;
using SimpleAPI.Services;

namespace SimpleAPI.Tests.CustomerServiceTests
{
    public abstract class CustomerServiceTestBase
    {
        public static ICustomerService MockNewCustomerService()
        {
            var testDbContext = Helper.GetIsolatedInMemoryContext();
            return new CustomerService(testDbContext);
        }
    }
}
