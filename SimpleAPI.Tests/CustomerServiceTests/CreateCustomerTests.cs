using SimpleAPI.Exceptions;
using SimpleAPI.Models;
using SimpleAPI.Services;
using System;
using System.Threading.Tasks;
using SimpleAPI.Constants;
using Xunit;

namespace SimpleAPI.Tests.CustomerServiceTests
{
    public class CreateCustomerTests : CustomerServiceTestBase
    {
     
        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.CreateAsync))]
        public async Task CreateCustomerAsync_GivenValidModel_ReturnsOneRowCreated()
        {
            // Arrange 
            var customerService = MockNewCustomerService();
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };

            var customerId = customer.Id;


            // Act
            var result = await customerService.CreateAsync(customer);
            var customerIdAfterCreated = customer.Id;
            var createdCustomer = await customerService.GetByIdAsync(customerId);

            // Assert
            Assert.Equal(1, result);
            Assert.Equal(customerId, customerIdAfterCreated);
            Assert.NotNull(createdCustomer);
            Assert.Equal(customer.FirstName, createdCustomer.FirstName);
            Assert.Equal(customer.LastName, createdCustomer.LastName);
            Assert.Equal(customer.DateOfBirth, createdCustomer.DateOfBirth);
        }


        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.CreateAsync))]
        public async Task CreateCustomerAsync_GivenEmptyId_ReturnsOneRowCreated()
        {
            // Arrange 
            var customerService = MockNewCustomerService();
            var customer = new Customer
            {
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };

            var customerId = customer.Id;
            
            // Act
            var result = await customerService.CreateAsync(customer);
            var customerIdAfterCreated = customer.Id;
            var createdCustomer = await customerService.GetByIdAsync(customerIdAfterCreated);

            // Assert
            Assert.Equal(1, result);
            Assert.NotEqual(customerId, customerIdAfterCreated);
            Assert.NotNull(createdCustomer);
            Assert.Equal(customer.FirstName, createdCustomer.FirstName);
            Assert.Equal(customer.LastName, createdCustomer.LastName);
            Assert.Equal(customer.DateOfBirth, createdCustomer.DateOfBirth);
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.CreateAsync))]
        public async Task CreateCustomerAsync_GivenExistingId_ThrowsException()
        {
            // Arrange 
            var customerService = MockNewCustomerService();
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };

            var customerId = customer.Id;

            await customerService.CreateAsync(customer);

            var customerWithSameId = new Customer
            {
                Id = customerId,
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };
            
            // Act & assert
            var exception =
                await Assert.ThrowsAsync<InvalidOperationException>(() => customerService.CreateAsync(customerWithSameId));
            
            Assert.Contains("same key value", exception.Message);
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.CreateAsync))]
        public async Task CreateCustomerAsync_GivenEmptyDOB_ReturnsOneRowCreated()
        {
            // Arrange 
            var customerService = MockNewCustomerService();
            var customer = new Customer
            {
                FirstName = "Alice",
                LastName = "Smith"
            };

            // Act
            var result = await customerService.CreateAsync(customer);
            var customerIdAfterCreated = customer.Id;
            var createdCustomer = await customerService.GetByIdAsync(customerIdAfterCreated);

            // Assert
            Assert.Equal(1, result);
            Assert.NotNull(createdCustomer);
            Assert.Equal(customer.FirstName, createdCustomer.FirstName);
            Assert.Equal(customer.LastName, createdCustomer.LastName);
            Assert.Null(createdCustomer.DateOfBirth);
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.CreateAsync))]
        public async Task CreateCustomerAsync_GivenEmptyFirstName_ThrowsException()
        {

            // Arrange 
            var customerService = MockNewCustomerService();
            var customer = new Customer
            {
                LastName = "Smith"
            };

          
            // Act & assert
            var exception =
                await Assert.ThrowsAsync<ServiceException>(() => customerService.CreateAsync(customer));

            Assert.Contains(Errors.FirstNameAndLastNameAreRequired, exception.Message);
        }

        [Fact]
        [Trait(nameof(CustomerService), nameof(CustomerService.CreateAsync))]
        public async Task CreateCustomerAsync_GivenEmptyLastName_ThrowsException()
        {
            // Arrange 
            var customerService = MockNewCustomerService();
            var customer = new Customer
            {
                FirstName = "Alice"
            };
            
            // Act & assert
            var exception =
                await Assert.ThrowsAsync<ServiceException>(() => customerService.CreateAsync(customer));
            
            Assert.Contains(Errors.FirstNameAndLastNameAreRequired, exception.Message);
        }
        
    }
}
