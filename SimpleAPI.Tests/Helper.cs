using Microsoft.EntityFrameworkCore;
using SimpleAPI.Data;
using System;

namespace SimpleAPI.Tests
{
    public static class Helper
    {
        public static SimpleApiContext GetIsolatedInMemoryContext()
        {
            var dynamicName = Guid.NewGuid().ToString();
            var options = new DbContextOptionsBuilder<SimpleApiContext>()
                .UseInMemoryDatabase(dynamicName);
            return new SimpleApiContext(options.Options);
        }

    }
}