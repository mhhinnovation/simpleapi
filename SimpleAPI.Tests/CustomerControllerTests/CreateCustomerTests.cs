using Microsoft.AspNetCore.Mvc;
using SimpleAPI.Exceptions;
using SimpleAPI.Models;
using System;
using System.Threading.Tasks;
using SimpleAPI.Constants;
using SimpleAPI.Controllers;
using Xunit;

namespace SimpleAPI.Tests.CustomerControllerTests
{
    public class CreateCustomerTests: CustomersControllerTestBase
    {
        [Fact]
        [Trait(nameof(CustomersController), nameof(CustomersController.CreateAsync))]
        public async Task CreateAsync_GivenValidModel_ReturnsNewCustomerWithProvidedId()
        {
            // Arrange 
           var controller = MockNewCustomerController();

            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };
            
            // Act
            var result = await controller.CreateAsync(customer);

            // Assert
            var createdResult = result as CreatedAtActionResult;
            Assert.NotNull(createdResult);

            var returnCustomer = Assert.IsType<Customer>(createdResult.Value);

            Assert.Equal(customer.Id, returnCustomer.Id);
            Assert.Equal(customer.FirstName, returnCustomer.FirstName);
            Assert.Equal(customer.LastName, returnCustomer.LastName);
            Assert.Equal(customer.DateOfBirth, returnCustomer.DateOfBirth);
        }

        [Fact]
        [Trait(nameof(CustomersController), nameof(CustomersController.CreateAsync))]
        public async Task CreateAsync_GivenEmptyId_ReturnsNewCustomerWithRandomId()
        {
            // Arrange 
            var controller = MockNewCustomerController();

            var customer = new Customer
            {
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };


            Assert.Equal(Guid.Empty, customer.Id);

            // Act
            var result = await controller.CreateAsync(customer);

            // Assert
            var createdResult = result as CreatedAtActionResult;
            Assert.NotNull(createdResult);

            var returnCustomer = Assert.IsType<Customer>(createdResult.Value);

            Assert.NotEqual(Guid.Empty, returnCustomer.Id);
            Assert.Equal(customer.FirstName, returnCustomer.FirstName);
            Assert.Equal(customer.LastName, returnCustomer.LastName);
            Assert.Equal(customer.DateOfBirth, returnCustomer.DateOfBirth);
        }

        [Fact]
        [Trait(nameof(CustomersController), nameof(CustomersController.CreateAsync))]
        public async Task CreateAsync_GivenExistingId_ReturnsInternalServerException()
        {
            // Arrange 

            var controller = MockNewCustomerController();
            var customerId = Guid.NewGuid();
            var customer = new Customer
            {
                Id = customerId,
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };

           await controller.CreateAsync(customer);

            var customerWithSameId = new Customer
            {
                Id = customerId,
                FirstName = "Alice",
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };


            // Act & assert
            var exception =
                await Assert.ThrowsAsync<HttpResponseException>(() => controller.CreateAsync(customerWithSameId));

            Assert.Equal(500,exception.Status);
            Assert.Contains("same key value",exception.Message);
        }

        [Fact]
        [Trait(nameof(CustomersController), nameof(CustomersController.CreateAsync))]
        public async Task CreateAsync_GivenEmptyDOB_ReturnsNewCustomerWithNoDOB()
        {
            // Arrange 
            var controller = MockNewCustomerController();
            var customer = new Customer
            {
                FirstName = "Alice",
                LastName = "Smith"
            };

            // Act
            var result = await controller.CreateAsync(customer);

            // Assert
            var createdResult = result as CreatedAtActionResult;
            Assert.NotNull(createdResult);

            var returnCustomer = Assert.IsType<Customer>(createdResult.Value);

            Assert.Equal(customer.FirstName, returnCustomer.FirstName);
            Assert.Equal(customer.LastName, returnCustomer.LastName);
            Assert.Null(returnCustomer.DateOfBirth);
        }

        [Fact]
        [Trait(nameof(CustomersController), nameof(CustomersController.CreateAsync))]
        public async Task CreateAsync_GivenEmptyFirstName_ReturnsBadRequest()
        {
            // Arrange 
            var controller = MockNewCustomerController();

            controller.ModelState.AddModelError("FirstName", Errors.FirstNameAndLastNameAreRequired);
            var customer = new Customer
            {
                FirstName = string.Empty,
                LastName = "Smith",
                DateOfBirth = DateTime.Parse("20/07/1970")
            };
            // Act
            var result = await controller.CreateAsync(customer);

            // Assert

            var badRequestResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestResult);
        }


        [Fact]
        [Trait(nameof(CustomersController), nameof(CustomersController.CreateAsync))]
        public async Task CreateAsync_GivenEmptyLastName_ReturnsBadRequest()
        {
            // Arrange 
            var controller = MockNewCustomerController();

            controller.ModelState.AddModelError("LastName", Errors.FirstNameAndLastNameAreRequired);
            var customer = new Customer
            {
                FirstName = "Alice",
                LastName = string.Empty,
                DateOfBirth = DateTime.Parse("20/07/1970")
            };

            // Act
            var result = await controller.CreateAsync(customer);

            // Assert
            var badRequestResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestResult);
        }
    }
}
