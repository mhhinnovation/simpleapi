using SimpleAPI.Controllers;
using SimpleAPI.Services;

namespace SimpleAPI.Tests.CustomerControllerTests
{
    public abstract class CustomersControllerTestBase
    {
        public static CustomersController MockNewCustomerController()
        {
            var testDbContext = Helper.GetIsolatedInMemoryContext();
            var mockCustomerService =  new CustomerService(testDbContext);
            return new CustomersController(mockCustomerService);
        }


    }
}