namespace SimpleAPI.Constants
{
    public static class Errors
    {
        public const string NameForSearchingAreRequired = "Name for searching cannot be null or empty.";

        public const string FirstNameAndLastNameAreRequired = "FirstName and LastName are required";

        public const string CannotFindCustomerWithMatchingId = "Cannot find customer with matching Id";
    }

}
