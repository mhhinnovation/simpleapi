﻿using Microsoft.EntityFrameworkCore;
using SimpleAPI.Constants;
using SimpleAPI.Data;
using SimpleAPI.Exceptions;
using SimpleAPI.Interfaces;
using SimpleAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleAPI.Services
{

    public class CustomerService: ICustomerService
    { 
        private readonly SimpleApiContext _context;

        public CustomerService(SimpleApiContext context)
        {
            _context = context;
        }



        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await _context.Customers
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            return await _context.Customers
                .FindAsync(id);
        }


        public async Task<IEnumerable<Customer>>GetByNameAsync(string partialName)
        {
            if (string.IsNullOrEmpty(partialName))
                throw new ServiceException(Errors.NameForSearchingAreRequired);

            return await _context.Customers
                .Where(c =>
                        c.FullName.Contains(partialName, StringComparison.OrdinalIgnoreCase))
                .ToListAsync();
        }


        public async Task<int> CreateAsync(Customer customer)
        {
            if (string.IsNullOrEmpty(customer.FirstName) || string.IsNullOrEmpty(customer.LastName))
                throw new ServiceException(Errors.FirstNameAndLastNameAreRequired);

            _context.Customers.Add(customer);
            return await _context.SaveChangesAsync();
                
        }


        public async Task<int> UpdateAsync(Customer customer)
        {
            _context.Entry(customer).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }



        public async Task<int> DeleteAsync(Guid id)
        {
            var customer = await _context.Customers.FindAsync(id);

            if (customer == null) 
                throw new ServiceException(Errors.CannotFindCustomerWithMatchingId);

            _context.Customers.Remove(customer);
            return await _context.SaveChangesAsync();

        }
    }
}