﻿using Microsoft.EntityFrameworkCore;
using SimpleAPI.Models;

namespace SimpleAPI.Data
{
    public class SimpleApiContext : DbContext
    {
        public SimpleApiContext(DbContextOptions<SimpleApiContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}