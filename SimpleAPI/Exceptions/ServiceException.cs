﻿using System;

namespace SimpleAPI.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceException(string message):base(message)
        {
        }
    }
}