﻿using System;

namespace SimpleAPI.Exceptions
{
    public class HttpResponseException : Exception
    {
        public HttpResponseException(string message, int status = 500) : base(message)
        {
            Value = message;
            Status = status;
        }

        public int Status { get; set; }

        public object Value { get; set; }
    }
}