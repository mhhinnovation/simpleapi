﻿using Microsoft.AspNetCore.Mvc;
using SimpleAPI.Interfaces;
using SimpleAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ApiControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }


        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAllAsync()
        {
            return await MakeServiceCall(async () =>
            {

                var customers = await _customerService.GetAllAsync();

                return Ok(customers);
            });
        }


        [HttpGet("GetById")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            return await MakeServiceCall(async () =>
            {
                var customer = await _customerService.GetByIdAsync(id);

                if (customer == null)
                {
                    return NotFound();
                }

                return Ok(customer);
            });
        }


        [HttpGet("SearchByName")]
        public async Task<IActionResult> SearchAsync(string partialName)
        {
            return await MakeServiceCall(async () =>
            {
                var customers = await _customerService.GetByNameAsync(partialName);

                if (customers.Any())
                {
                    return Ok(customers);
                }

                return NotFound();
            });
        }


        [HttpPost("Create")]
        public async Task<IActionResult> CreateAsync(Customer customer)
        {
            return await MakeServiceCall(async () =>
            {
                var createdResult = await _customerService.CreateAsync(customer);

                if (createdResult < 1)
                {
                    return BadRequest();
                }

                return CreatedAtAction(nameof(GetAsync), new { id = customer.Id }, customer);
            });
        }


        [HttpPut("Update")]
        public async Task<IActionResult> UpdateAsync(Guid id, Customer customer)
        {
            return await MakeServiceCall(async () =>
            {
                if (id != customer.Id)
                {
                    return BadRequest();
                }

                var updatedResult = await _customerService.UpdateAsync(customer);

                if (updatedResult < 1)
                {
                    return BadRequest();
                }

                return NoContent();
            });
        }



        [HttpDelete("Delete")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            return await MakeServiceCall(async () =>
            {
                var deletedResult = await _customerService.DeleteAsync(id);

                if (deletedResult < 1)
                {

                    return BadRequest();
                }

                return NoContent();
            });
        }
    }
}