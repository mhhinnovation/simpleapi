﻿using Microsoft.AspNetCore.Mvc;
using SimpleAPI.Exceptions;
using System;
using System.Threading.Tasks;

namespace SimpleAPI.Controllers
{
    public abstract class ApiControllerBase : ControllerBase
    {
        //Model validation and http response exception are handled by middleware
        protected async Task<IActionResult> MakeServiceCall(Func<Task<IActionResult>> callback)
        {
            try
            {
                return await callback();

            }
            catch (ServiceException serviceException)
            {
                return BadRequest(serviceException);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception.Message);
            }
        }
    }
}